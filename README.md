This project will deploy fine with dfx version 0.7.0-beta.2.

With dfx version 0.7.0-beta.3, this will return this error:
```
The invocation to the wallet call forward method failed with the error:

An error happened during the call: 5:
    Wasm module of canister rrkah-fqaaa-aaaaa-aaaaq-cai is not valid: 
        Wasm module defined 6392 functions which excees the maximum number allowed 6000.
```

Requirements to build:
- `ic_cdk_optimizer` binary on path
- Dors must be installed with `cargo install dors`