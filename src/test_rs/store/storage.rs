pub(crate) mod storage {
    use std::collections::HashMap;

    use ic_cdk::{println, storage};
    use ic_cdk::export::candid::CandidType;
    use ic_cdk::export::Principal;
    use serde::Deserialize;

    #[derive(Clone, Debug, Deserialize, CandidType, Default)]
    pub(crate) struct Storage {
        pub data: HashMap<Principal, User>,
    }

    impl Storage {
        pub(crate) fn get_mut() -> &'static mut Storage {
            storage::get_mut::<Storage>()
        }

        pub(crate) fn get() -> &'static Storage {
            storage::get::<Storage>()
        }

        pub(crate) fn save() {
            let store = Storage::get();
            println!("Saving storage to stable memory..");
            storage::stable_save((store, )).unwrap();
        }

        pub(crate) fn restore() {
            let (old_store, ): (Storage, ) = storage::stable_restore()
                .expect("Could not restore Storage from stable memory.");
            let current_store = Storage::get_mut();
            current_store.data = old_store.data
        }
    }

    #[derive(Debug, Clone, Deserialize, CandidType, Default)]
    pub(crate) struct User {
        id: u32,
        foo_field: String,
        foo_optional: Option<Bar>,
    }

    #[derive(Debug, Clone, Deserialize, CandidType, Default)]
    pub(crate) struct Bar {
        id: u32,
        key: Option<String>,
        expiration: Option<u64>,
    }
}