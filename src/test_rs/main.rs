use ic_cdk_macros::*;

use crate::store::storage::storage::{Storage, User};

mod store;

#[query]
fn get_user() -> Option<User> {
    Storage::get().data.get(&ic_cdk::caller()).cloned()
}

#[init]
fn init() {}

#[pre_upgrade]
fn pre_upgrade() {
    Storage::save()
}

#[post_upgrade]
fn post_upgrade() {
    Storage::restore()
}